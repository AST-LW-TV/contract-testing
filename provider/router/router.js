const express = require("express");
const router = express.Router();

const { getDetailsOfStudent, getAllStudentsList } = require("../controllers/studentDetailsLogic");

router.route("/students/details").get(getDetailsOfStudent);
router.route("/students").get(getAllStudentsList);

module.exports = router;
