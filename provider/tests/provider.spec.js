require("dotenv").config();
const { Verifier } = require("@pact-foundation/pact");
const path = require("path");
const fileSystem = require("fs");

// checking if the output directory is present or not
const checkDirAndWrite = (data) => {
    if (!fileSystem.existsSync("./provider/output")) {
        fileSystem.mkdirSync("./provider/output", { recursive: true });
    }
    // writing the output after running the tests to result.txt file
    fileSystem.writeFileSync(path.resolve(process.cwd(), "provider", "output", "result.txt"), data, (err) => {
        console.log(err);
    });
};

// Server should be running to validate the tests
// If the server is not running, then throws error ...

describe("Student Details API", () => {
    it("Verify all the expected responses of student details APIs", () => {
        const options = {
            providerBaseUrl: `http://localhost:${process.env.PORT}`, // getting the port number from .env file
            provider: "StudentDetailsServer",
            pactUrls: [path.resolve(__dirname, "../../pacts/studentdetailsclient-studentdetailsserver.json")],
        };
        return new Verifier(options)
            .verifyProvider()
            .then((output) => {
                checkDirAndWrite(output);
            })
            .catch((error) => {
                // console.log(error) // for best visualization...
                checkDirAndWrite(error.message);
                throw new Error("Failures in interactions");
            });
    });
});
