// Lets assume the server side application handles only one get request
// API endpoint -> /api/v1/students/detail/:id
// RETURNS      -> student detail when we hit API endpoint

require("dotenv").config();
const express = require("express");

const studentApiRouter = require("./router/router");

const app = express();

app.use(express.json());

app.use(`/api/${process.env.VERSION}`, studentApiRouter);

const port = process.env.PORT;

// start the server to listen to the specified port number ...
app.listen(port, (err) => {
    if (err) {
        console.log("Error in starting the server");
    }
    console.log(`Server started at ${port}... `);
});
