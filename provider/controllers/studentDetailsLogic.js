const studentDetails = require("../db/studentDetails");

const getDetailsOfStudent = (req, res) => {
    // in version-2 instead of using path parameter, I am changing to query param...
    const id = parseInt(req.query.id);

    const studentInformation = studentDetails.find((item) => item.id === id);
    // if the id is provided but the resource with the given id is not present or not created ...
    // id = 1000
    if (!studentInformation) {
        return res.status(404).json({
            message: "The resource with the given id is not found",
            success: false,
        });
    }
    // return the student detail if everything goes correct ...
    return res.status(200).json({
        studentInformation,
        success: true,
    });
};

const getAllStudentsList = (req, res) => {
    return res.status(200).json({ studentDetails, success: true });
};

module.exports = {
    getDetailsOfStudent,
    getAllStudentsList,
};
