require("dotenv").config();
const { Pact } = require("@pact-foundation/pact");
const path = require("path");

const chai = require("chai");
const { expect } = require("chai");

chai.use(require("chai-as-promised"));

const { getStudentDetails, getAllStudentsList } = require("../appClientSide");
const { eachLike } = require("@pact-foundation/pact/src/dsl/matchers");

const VERSION_NUMBER = process.env.VERSION;

const provider = new Pact({
    port: parseInt(process.env.PACT_PORT_NUMBER), // the value will be read in string format... so convert to number format
    consumer: "StudentDetailsClient",
    provider: "StudentDetailsServer",
    log: path.resolve(process.cwd(), "logs", "pact.log"),
    dir: path.resolve(process.cwd(), "pacts"),
});

describe("Students Details API ", () => {
    before(async () => {
        await provider.setup();
    });

    afterEach(async () => {
        await provider.verify();
    });

    after(async () => {
        await provider.finalize();
    });

    it("Validate the details of a Student with the given / specific ID", async () => {
        // Arrange
        await provider.addInteraction({
            state: "ID of student exists - Student ID: 1",
            uponReceiving: "Get the student details",
            withRequest: {
                method: "GET",
                path: `/api/${VERSION_NUMBER}/students/details`,
                query: {
                    id: "1",
                },
            },
            willRespondWith: {
                status: 200,
                body: {
                    studentInformation: {
                        id: 1,
                        name: "John",
                        class: 4,
                    },
                    success: true,
                },
            },
        });
        // Act
        const response = await getStudentDetails(1);
        const data = response.data;

        // Assert
        expect(data.studentInformation.id).equal(1);
        expect(data.studentInformation.name).equal("John");
        expect(data.studentInformation.class).equal(4);
    });

    it("Validate the list of all students", async () => {
        // Arrange
        await provider.addInteraction({
            uponReceiving: "Get all the students list",
            withRequest: {
                method: "GET",
                path: `/api/${VERSION_NUMBER}/students`,
            },
            willRespondWith: {
                status: 200,
                body: {
                    studentDetails: eachLike({
                        id: 1,
                        name: "John",
                        class: 4,
                    }),
                    success: true,
                },
            },
        });

        // Act
        const response = await getAllStudentsList();
        const data = response.data;

        // Assert
        expect(data.studentDetails.length).to.be.greaterThan(0);
    });

    it("Validate 404 status code and success value if the requested resource not found", async () => {
        // Arrange
        await provider.addInteraction({
            state: "ID should be greater than the length of the student list - Student ID - 12",
            uponReceiving: "Get not found error and success value to be false",
            withRequest: {
                path: `/api/${VERSION_NUMBER}/students/details`,
                method: "GET",
                query: {
                    id: "12",
                },
            },
            willRespondWith: {
                status: 404,
                body: {
                    message: "The resource with the given id is not found",
                    success: false,
                },
            },
        });

        // Act and Assert

        await expect(getStudentDetails(12)).to.be.eventually.rejectedWith("Request failed with status code 404");
    });
});
