// dependencies
require("dotenv").config();
const axios = require("axios").default;

// environment variable
const PACT_PORT_NUMBER = process.env.PACT_PORT_NUMBER;
const VERSION_NUMBER = process.env.VERSION;

const url = `http://localhost:${PACT_PORT_NUMBER}`;

const getAllStudentsList = () => {
    return axios.get(`${url}/api/${VERSION_NUMBER}/students`).then((response) => {
        return {
            data: response.data,
            statusCode: response.status,
        };
    });
};

const getStudentDetails = (ID) => {
    return axios
        .get(`${url}/api/${VERSION_NUMBER}/students/details`, {
            params: {
                id: ID,
            },
        })
        .then((response) => {
            return {
                data: response.data,
                statusCode: response.status,
            };
        });
};

module.exports = {
    getStudentDetails,
    getAllStudentsList,
};
